﻿using System.Collections.Generic;
using System.Linq;
namespace TestingCode
{
    public class Account
    {
        public int accountId { get; set; }
        public string customerNum { get; set; }        
        public int balance { get; set; }
        public int overdraft { get; set; }
        public string AccountType { get; set; }
        public List<Account> _accList = new List<Account>();

        public void populateList()
        {            
            var meme0 = new Account { accountId = 1, customerNum = "1", balance = 2000, overdraft = 0, AccountType = "Savings" };
            var meme1 = new Account { accountId = 2, customerNum = "2", balance = 5000, overdraft = 0, AccountType = "Savings" };
            var meme2 = new Account { accountId = 3, customerNum = "3", balance = 1000, overdraft = -10000, AccountType = "Current" };
            var meme3 = new Account { accountId = 4, customerNum = "4", balance = 5000, overdraft = -20000, AccountType = "Current" };
            _accList.Add(meme0);
            _accList.Add(meme1);
            _accList.Add(meme2);
            _accList.Add(meme3);
        }

        public Account getAccountById(int accId)
        {
            Account meme = new Account();
            if (_accList.Count > 0)
            {
                foreach (var meme1 in _accList)
                {
                    if (meme1.accountId == accId)
                    {
                        meme = meme1;
                    }
                }
            }
            return meme;
        }
    }
}
