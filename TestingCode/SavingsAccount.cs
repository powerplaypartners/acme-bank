﻿using System;

namespace TestingCode
{
    class SavingsAccount : AccountService
    {
        //Deposit process for savings account
        public void Deposit(int amount, Account x)
        {            
            Console.WriteLine("Type: Savings Account Deposit");
            Console.WriteLine("Old Balance: R" + x.balance);
            Console.WriteLine("Deposit Amount: R" + amount);
            x.balance += Convert.ToInt32(amount);
            Console.WriteLine("New Balance: R" + x.balance);
            Console.WriteLine("Press any key to exit");
        }

        //Withdrawal process for savings account
        public void Withdraw(int amount, Account x)
        {     
            //Balance restiction on savings account       
            if (x.balance - amount >= 1000)
            {
                Console.WriteLine("Savings Withdraw: R" + amount);
                Console.WriteLine("Old Balance: R" + x.balance);
                x.balance -= Convert.ToInt32(amount);
                Console.WriteLine("New Balance: R" + x.balance);
                Console.WriteLine("Press any key to exit");
            }
            else
            {
                Console.WriteLine("Insufficient funds");
                Console.WriteLine("Available funds: R" + x.balance);
                Console.WriteLine("Press any key to exit");

            }            
        }
    }
}
