﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestingCode
{
    interface AccountService
    {
        //void openSavingsAccount(int accountID, int amountToDeposit);
        //void openCurrentAccount(int accountID);
        void Withdraw(int amountToWithdraw, Account x);            
        void Deposit(int amountToDeposit, Account x);
    }
}
