﻿using System;

namespace TestingCode
{
    class MainFormControl
    {
        private AccountService CurrentBanking = new CurrentAccount();
        private AccountService SavingsBanking = new SavingsAccount();
        Account acc = new Account();

        private void prepareDB_SetAccount(int accountID)
        {
            acc.populateList();
            acc = acc.getAccountById(Convert.ToInt32(accountID));
        }

        public void runProgram()
        {
            Console.WriteLine("WELCOME TO ACME BANK");
            Console.WriteLine("--------------------");
            Console.WriteLine("Please specify your transaction");
            Console.WriteLine("[1] Deposit");
            Console.WriteLine("[2] Withdraw");
            Console.WriteLine("");
            string _action = Console.ReadLine();

            while (_action != "1" && _action != "2")
            {
                Console.WriteLine("Invalid option.\nPlease select option 1 or 2.");
                _action = Console.ReadLine();
            }

            switch (_action)
            {
                case "1":
                    _action = "Deposit";
                    break;
                case "2":
                    _action = "Withdraw";
                    break;
            }
            //Enter a valid account number
            Console.WriteLine("{0}:\nPlease enter a valid account number: ", _action);
            string _accNum = Console.ReadLine();
            int accNum = 0;
            bool isValidAccountNumber = int.TryParse(_accNum, out accNum);

            while (!isValidAccountNumber)
            {
                Console.WriteLine("Invalid account number.\n{0}: Please enter a valid account number: ", _action);
                _accNum = Console.ReadLine();
                isValidAccountNumber = int.TryParse(_accNum, out accNum);
            }

            //Enter a valid amount
            Console.WriteLine("Please enter a valid {0} amount: ", _action);
            string _amount = Console.ReadLine();
            int amount = 0;
            bool isValidAmount = int.TryParse(_amount, out amount);

            while (!isValidAmount)
            {
                Console.WriteLine("Invalid amount.\nPlease enter a valid {0} amount: R", _action);
                _amount = Console.ReadLine();
                isValidAmount = int.TryParse(_amount, out amount);
            }

            prepareDB_SetAccount(accNum);

            doDepositOrWithdrawal(_action, amount);
            Console.ReadLine();
        }

        public void doDepositOrWithdrawal(string _action, int _amount)
        {
            switch(acc.AccountType)
            {
                case "Savings":
                    if (_action == "Deposit")
                        SavingsBanking.Deposit(_amount, acc);
                    if (_action == "Withdraw")
                        SavingsBanking.Withdraw(_amount, acc);
                    break;
                case "Current":
                    if (_action == "Deposit")
                        CurrentBanking.Deposit(_amount, acc);
                    if (_action == "Withdraw")
                        CurrentBanking.Withdraw(_amount, acc);
                    break;
            }
            
        }
    }
}
