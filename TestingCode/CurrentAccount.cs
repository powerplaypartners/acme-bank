﻿using System;

namespace TestingCode
{
    class CurrentAccount : AccountService
    {
        public void Deposit(int amount, Account x)
        {
            Console.WriteLine("Type: Current Account Deposit");
            Console.WriteLine("Old Balance: R" + x.balance);
            Console.WriteLine("Deposit Amount: R" + amount);
            x.balance += Convert.ToInt32(amount);
            Console.WriteLine("New Balance: R" + x.balance);
            Console.WriteLine("Press any key to exit");
        }
  
        public void Withdraw(int amount, Account x)
        {
            //Balance restiction on current account       
            if (x.balance - amount >= x.overdraft)
            {
                Console.WriteLine("Current Account Withdraw: R" + amount);
                Console.WriteLine("Old Balance: R" + x.balance);
                x.balance -= Convert.ToInt32(amount);
                Console.WriteLine("New Balance: R" + x.balance);
                Console.WriteLine("Press any key to exit");
            }
            else
            {
                Console.WriteLine("Insufficient funds");
                Console.WriteLine("Current Balance is only: R" + x.balance + ", overdraft limit is: R" + Math.Abs(x.overdraft) + ".\nPlease select a smaller amount to withdraw ");
                Console.WriteLine("Press any key to exit");
            }
        }
    }
}
